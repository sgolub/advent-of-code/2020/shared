package gameconsole

import (
	"fmt"
	"strconv"
	"strings"
)

// Instruction is a command for the HandheldGameConsole
type Instruction struct {
	Cmd string
	Arg int
	Run bool
}

// NewInstructionFromString parses string input to a new Instruction
func NewInstructionFromString(val string) (*Instruction, error) {
	splits := strings.Split(val, " ")
	i := &Instruction{}
	if len(splits) != 2 {
		return i, fmt.Errorf("invalid instruction string: %s", val)
	}
	switch splits[0] {
	case cmdAcc:
		fallthrough
	case cmdJump:
		fallthrough
	case cmdNoOp:
		i.Cmd = splits[0]
	default:
		return i, fmt.Errorf("invalid instruction command: %s", splits[0])
	}
	arg, err := strconv.Atoi(splits[1])
	if err != nil {
		return i, fmt.Errorf("invalid instruction arg: %w", err)
	}
	i.Arg = arg
	return i, nil
}
