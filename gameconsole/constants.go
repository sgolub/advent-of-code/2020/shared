package gameconsole

const (
	cmdAcc  = "acc"
	cmdJump = "jmp"
	cmdNoOp = "nop"

	bigDot = "\u25cf"
)
