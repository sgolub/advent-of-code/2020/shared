package gameconsole

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
)

var (
	done = make(chan struct{})
	wg   sync.WaitGroup
)

// HandheldGameConsole is a game console computer
type HandheldGameConsole struct {
	Position    int
	Accumulator int
	Program     []*Instruction
	Debug       bool
}

// NewHandheldGameConsole creates a new instance of HandheldGameConsole
func NewHandheldGameConsole(instructions []*Instruction) *HandheldGameConsole {
	return &HandheldGameConsole{
		Program: instructions,
		Debug:   os.Getenv("COMPUTER_DEBUG") == "true",
	}
}

// SetProgram will re-write the program established
func (g *HandheldGameConsole) SetProgram(instructions []*Instruction) {
	g.Program = instructions
}

// Log will output if debug is true
func (g *HandheldGameConsole) Log(params ...interface{}) {
	if g.Debug {
		fmt.Println(params...)
	}
}

// Iterate will advance the logic necessary for the computer. Returns a bool indicating whether the loop should continue
func (g *HandheldGameConsole) Iterate() bool {
	if g.Position >= len(g.Program) {
		return false
	}
	instr := g.Program[g.Position]
	if instr.Run {
		g.Log("Dupe", g.Position, instr)
		return false
	}
	g.Log(g.Position, instr)
	switch instr.Cmd {
	case cmdAcc:
		g.Accumulator += instr.Arg
		fallthrough
	case cmdNoOp:
		g.Position++
	case cmdJump:
		g.Position += instr.Arg
	}
	if g.Position >= len(g.Program) {
		return false
	}
	instr.Run = true
	return true
}

// Run will execute the program and return the result
func (g *HandheldGameConsole) Run() int {
	for {
		if !g.Iterate() {
			break
		}
	}
	return g.Accumulator
}

func createView(gui *gocui.Gui, viewName string, x, y, w, h int) (*gocui.View, error) {
	v, err := gui.SetView(viewName, x, y, w, h)
	if err != nil && err != gocui.ErrUnknownView {
		return nil, err
	}
	return v, nil
}

type layoutManager struct {
	computer *HandheldGameConsole
	maxX     int
	maxY     int
}

func (g *HandheldGameConsole) newLayoutManager(gui *gocui.Gui) (layoutManager, error) {
	maxX, maxY := gui.Size()
	l := layoutManager{
		computer: g,
		maxX:     maxX,
		maxY:     maxY,
	}

	return l, nil
}

func getCmdColorFunc(cmd string, emphasize bool) func(...interface{}) string {
	var c *color.Color
	switch cmd {
	case cmdJump:
		c = color.New(color.FgBlue)
	case cmdNoOp:
		c = color.New(color.FgRed)
	case cmdAcc:
		c = color.New(color.FgYellow)
	}
	if emphasize {
		c = c.Add(color.Underline, color.Bold)
	}
	return c.SprintFunc()
}

func (l layoutManager) Layout(gui *gocui.Gui) error {
	vInfobar, err := createView(gui, "infobar", 0, 0, l.maxX-29, 2)
	if err != nil {
		return err
	}
	vInfobar.Overwrite = true
	vInfobar.Autoscroll = true

	vStatusbar, err := createView(gui, "statusbar", l.maxX-28, 0, l.maxX-1, 2)
	if err != nil {
		return err
	}
	vStatusbar.Overwrite = true
	vStatusbar.Autoscroll = true

	vCmdcol, err := createView(gui, "cmdcol", 0, 3, 11, l.maxY-1)
	if err != nil {
		return err
	}
	vCmdcol.Autoscroll = true

	vCurindex, err := createView(gui, "curindexcol", l.maxX-7, 3, l.maxX-1, l.maxY-1)
	if err != nil {
		return err
	}
	vCurindex.Autoscroll = true

	vGrid, err := createView(gui, "grid", 12, 3, l.maxX-8, l.maxY-1)
	if err != nil {
		return err
	}
	vGrid.Overwrite = true
	if !l.computer.Iterate() {
		fmt.Fprintf(vInfobar, "\nThe result is %d", l.computer.Accumulator)
		return nil
	}
	vGrid.Wrap = true
	fmt.Fprintf(vInfobar, "\nDetecting Cycle...")
	fmt.Fprintf(vStatusbar, "\nADDRESS: %04d | ACC: %06d", l.computer.Position, l.computer.Accumulator)
	fmt.Fprintln(vCurindex, l.computer.Position)
	cmd := l.computer.Program[l.computer.Position]
	cmdFuncMap := map[string]func(...interface{}) string{
		cmdAcc:  getCmdColorFunc(cmdAcc, false),
		cmdJump: getCmdColorFunc(cmdJump, false),
		cmdNoOp: getCmdColorFunc(cmdNoOp, false),
	}

	fmt.Fprintf(vCmdcol, "%s %6d\n", cmdFuncMap[cmd.Cmd](cmd.Cmd), cmd.Arg)

	vGrid.Clear()
	gx, _ := vGrid.Size()
	maxSizeX := gx / 2
	gridText := []string{}
	for i, c := range l.computer.Program {
		if len(gridText) == maxSizeX {
			fmt.Fprintln(vGrid, strings.Join(gridText, " "))
			gridText = []string{}
		}
		sf := cmdFuncMap[c.Cmd]
		if i == l.computer.Position {
			sf = getCmdColorFunc(c.Cmd, true)
		}
		gridText = append(gridText, sf(bigDot))
	}
	fmt.Fprintln(vGrid, strings.Join(gridText, " "))
	return nil
}

func counter(g *gocui.Gui) {
	defer wg.Done()

	for {
		select {
		case <-done:
			return
		case <-time.After(100 * time.Millisecond):
			g.Update(func(g *gocui.Gui) error {
				return nil
			})
		}
	}
}

// RunWithUI will run the computer with a fancy ui
func (g *HandheldGameConsole) RunWithUI() {
	gui, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		fmt.Println("failed to establish ui:", err)
		return
	}
	defer gui.Close()
	lm, err := g.newLayoutManager(gui)
	if err != nil {
		fmt.Println("failed to build layoutmanager:", err)
	}
	gui.SetManager(lm)

	if err := gui.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln("failed keybinding", err)
	}
	if err := gui.SetKeybinding("", 'q', gocui.ModNone, quit); err != nil {
		log.Panicln("failed keybinding", err)
	}

	go counter(gui)
	if err := gui.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln("error received during execution:", err)
	}
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}
