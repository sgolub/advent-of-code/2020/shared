module gitlab.com/sgolub/advent-of-code/2020/shared/v2

go 1.15

require (
	github.com/fatih/color v1.10.0
	github.com/jroimartin/gocui v0.4.0
	github.com/nsf/termbox-go v0.0.0-20201124104050-ed494de23a00 // indirect
)
